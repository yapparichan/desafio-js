var questao;
var requestURL = "https://quiz-trainee.herokuapp.com/questions";
var request = new XMLHttpRequest();
request.open('GET', requestURL);

request.onreadystatechange = function(){
     if(request.readyState === 4){
          if(request.status === 200){
               questao = JSON.parse(request.responseText);
          }
     }
};

request.send(null);

var i=-1;
var pontuacao=0;
var prim=true;


var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der, for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
   },
   
]

function mostrarQuestao() {
    
    var check=0;
    for(var j=0;j<4;j++){
         if(document.getElementsByTagName('input')[j].checked==true){
              check=1;

         }
    }
    
    if(check==1 || prim==true){
         prim=false;
         
         i++;
         if(i>5){
              
              for(var j=0;j<4;j++){
                   if(document.getElementsByTagName('input')[j].checked==true){
                        pontuacao+=parseInt(document.getElementsByTagName('input')[j].value);
                   }
              }   
                      
              finalizarQuiz();
         }
         else{
              
              for(var j=0;j<4;j++){
                   if(document.getElementsByTagName('input')[j].checked==true){
                        pontuacao+=parseInt(document.getElementsByTagName('input')[j].value);
                   }
              }
              
              document.getElementById('confirmar').innerHTML= "Próxima";
              
              document.getElementById('listaRespostas').style.display="inline";
              
              document.getElementById('titulo').innerHTML= perguntas[i].texto;


              for(var j=0;j<4;j++){
                   
                   document.getElementsByClassName('titulo')[j].innerHTML=perguntas[i].respostas[j].texto;
                   
                   document.getElementsByTagName('input')[j].value=perguntas[i].respostas[j].valor;

              }
              
              for(var j=0;j<4;j++){
                   if(document.getElementsByTagName('input')[j].checked){
                        document.getElementsByTagName('input')[j].checked=false;
                   }
              }
         }
    }

}

function finalizarQuiz() {
    
    document.getElementById('listaRespostas').style.display="none";
    
    document.getElementById('titulo').innerHTML= "Fim!<br>Sua pontuação foi: "+parseInt((pontuacao*100)/18)+"%";
    
    document.getElementById('confirmar').innerHTML= "Jogar novamente";
    
    i=-1;
    pontuacao=0;
    
    for(var j=0;j<4;j++){
         if(document.getElementsByTagName('input')[j].checked){
              document.getElementsByTagName('input')[j].checked=false;
         }
    }
    
    prim=true;
}
